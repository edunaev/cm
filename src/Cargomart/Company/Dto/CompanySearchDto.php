<?php

namespace Cargomart\Company\Dto;

use Base\RequestInterface;

/**
 * DTO - не обязательно менять от версии к версии.
 * ИМХО, ничего страшного нет в том что здесь может быть много параметров, если они связаны.
 */
class CompanySearchDto
{
    private $id;
    private $name;

    private function __construct($id, $name)
    {
        $this->id   = $id;
        $this->name = $name;
    }

    public static function buildByHttpRequest(RequestInterface $request)
    {
        return new CompanySearchDto(
            (int) $request->getQueryParam('id'),
            (string) $request->getQueryParam('name')
        );
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }
}

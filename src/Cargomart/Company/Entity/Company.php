<?php

namespace Cargomart\Company\Entity;

/**
 * Сущность.
 */
class Company
{
    private $id;
    private $name;

    public static function buildByArray($data)
    {
        if (! $data) {
            return null;
        }

        $company = new Company();

        $company->setId($data['id'] ?? null);
        $company->setName($data['name'] ?? null);

        return $company;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
}

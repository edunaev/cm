<?php

namespace Cargomart\Company\Api\V1\Service;

use Cargomart\Company\Entity\Company;
use Cargomart\Company\Dto\CompanySearchDto;
use Cargomart\Company\Repository\CompanySearchRepository;

class CompanyViewService
{
    const API_ENTITY_NAME = 'company';

    /**
     * @var CompanySearchRepository
     */
    private $companyRepository;

    public function getCompany(Company $company)
    {
        return [
            self::API_ENTITY_NAME => $this->extract($company)
        ];
    }

    public function getCompanyByHash($hash)
    {
        $company = $this->companyRepository->findByHash($hash);
        if (! $company) {
            return [];
        }

        return $this->getCompany($company);
    }

    public function getCompanyList(CompanySearchDto $searchDto)
    {
        $companies = $this->companyRepository->search($searchDto);
        if (! $companies) {
            return [];
        }

        $result = [];
        foreach ($companies as $company) {
            $result[] = $this->extractListItem($company);
        }

        return [
            self::API_ENTITY_NAME => $result
        ];
    }

    protected function extract(Company $company)
    {
        return [
            'id'   => $company->getId(),
            'name' => $company->getName(),
        ];
    }

    protected function extractListItem(Company $company)
    {
        return $this->extract($company);
    }
}

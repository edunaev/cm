<?php

namespace Cargomart\Company\Api\V1\Controller;

use Base\Controller;
use Base\RequestInterface;
use Cargomart\Company\Api\V1\Service\CompanyViewService;
use Cargomart\Company\Dto\CompanySearchDto;

class CompanyViewController extends Controller
{
    /**
     * @var CompanyViewService
     */
    private $companyViewService;

    public function actionIndex(RequestInterface $request)
    {
        $searchDto = CompanySearchDto::buildByHttpRequest($request);

        return $this->jsonSuccess($this->companyViewService->getCompanyList($searchDto));
    }

    public function actionView($hash)
    {
        $company = $this->companyViewService->getCompanyByHash($hash);
        if (! $company) {
            return $this->jsonFail('Company not found');
        }

        return $this->jsonSuccess($company);
    }
}

<?php

namespace Cargomart\Company\Api\V1\Controller;

use Base\Controller;
use Base\RequestInterface;
use Cargomart\Company\Api\V1\Service\CompanyViewService;
use Cargomart\Company\Porter\ICompanyPorter;
use Cargomart\Company\Service\CompanyEditService;

/**
 * Здесь добавляется проверка доступа.
 *
 * @see CompanyEditController::isAccessDenied()
 */
class CompanyEditController extends Controller
{
    /**
     * @var CompanyEditService
     */
    private $companyEditService;
    /**
     * @var CompanyViewService
     */
    private $companyViewService;
    /**
     * @var ICompanyPorter
     */
    private $jsonCompanyPorter;

    public function actionCreate(RequestInterface $request)
    {
        if ($this->isAccessDenied()) {
            return $this->jsonFailAuth();
        }

        $company = $this->jsonCompanyPorter->import($request->getRawBody());

        try {
            $this->companyEditService->createCompany($company);
        } catch (\LogicException $logicException) {
            return $this->jsonFail($logicException->getMessage());
        }

        return $this->jsonSuccess($this->companyViewService->getCompany($company));
    }

    public function actionUpdate(RequestInterface $request)
    {
        if ($this->isAccessDenied()) {
            return $this->jsonFailAuth();
        }

        $company = $this->jsonCompanyPorter->import($request->getRawBody());

        try {
            $this->companyEditService->updateCompany($company);
        } catch (\LogicException $logicException) {
            return $this->jsonFail($logicException->getMessage());
        }

        return $this->jsonSuccess($this->companyViewService->getCompany($company));
    }

    /**
     * Необязательно писать этот код в контроллере, можно вынести в какой-то новый сервис.
     * Важна наглядность. Т.е., я хочу глядя в контроллер понимать какие проверки доступа здесь есть.
     */
    protected function isAccessDenied()
    {
        return false;
    }
}

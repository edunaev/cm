<?php

namespace Cargomart\Company\Service;

use Cargomart\Company\Entity\Company;
use Cargomart\Company\Repository\CompanyEditRepository;

/**
 * Сейчас он только вызывает методы репозиториев, но в будущем он пригодится как удобная прослойка.
 */
class CompanyEditService
{
    /**
     * @var CompanyEditRepository
     */
    private $companyEditRepository;

    public function createCompany(Company $company)
    {
        return $this->companyEditRepository->createCompany($company);
    }

    public function updateCompany(Company $company)
    {
        return $this->companyEditRepository->updateCompany($company);
    }
}

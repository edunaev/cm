<?php

namespace Cargomart\Company\Porter;

use Cargomart\Company\Entity\Company;

/**
 * Реализация для портирования из json'a в Company.
 */
class JsonCompanyPorter implements ICompanyPorter
{
    public function import($data): Company
    {
        return Company::buildByArray(
            json_decode($data, true)
        );
    }
}

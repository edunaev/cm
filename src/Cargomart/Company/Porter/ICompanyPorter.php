<?php

namespace Cargomart\Company\Porter;

use Cargomart\Company\Entity\Company;

/**
 * Портеры позволяют из входящих данных формировать знакомые нам объекты (не обязательно сущности, это могут быть DTO).
 * Также их можно использовать и для export'a (extract).
 */
interface ICompanyPorter
{
    public function import($data): Company;
}

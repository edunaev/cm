<?php

namespace Cargomart\Company\Repository;

use Cargomart\Company\Entity\Company;

/**
 * Разбил репозитории на два: поиск и редактирование (создание).
 *
 * Необязательно всегда так разбивать, но если поисковых методов становится слишком много и интерфейс становится
 * жирным, то лучше разнести.
 */
class CompanyEditRepository
{
    public function createCompany(Company $company)
    {
        return $company;
    }

    public function updateCompany(Company $company)
    {
        return $company;
    }
}

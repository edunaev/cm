<?php

namespace Cargomart\Company\Repository;

use Cargomart\Company\Dto\CompanySearchDto;
use Cargomart\Company\Entity\Company;

/**
 * Поисковый репозиторий по компаниям.
 *
 * Если метод search() станет слишком жирным, то можно выпихнуть его в отдельный интерфейс.
 */
class CompanySearchRepository
{
    public function findByHash($hash)
    {
        return new Company();
    }

    public function search(CompanySearchDto $searchDto)
    {
        return [
            new Company(),
            new Company()
        ];
    }
}

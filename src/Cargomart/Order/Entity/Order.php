<?php

namespace Cargomart\Order\Entity;

/**
 * Order entity.
 */
class Order
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $carrierId;
    /**
     * @var int
     */
    private $consignorId;

    /**
     * @param $data
     *
     * @return Order
     */
    public static function buildByArray($data)
    {
        $order = new Order();

        $order->setId($data['id'] ?? null);
        $order->setName($data['name'] ?? null);
        $order->setCarrierId($data['carrierId'] ?? null);
        $order->setConsignorId($data['consignorId'] ?? null);

        return $order;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCarrierId()
    {
        return $this->carrierId;
    }

    public function setCarrierId($carrierId)
    {
        $this->carrierId = $carrierId;
    }

    public function getConsignorId()
    {
        return $this->consignorId;
    }

    public function setConsignorId($consignorId)
    {
        $this->consignorId = $consignorId;
    }
}

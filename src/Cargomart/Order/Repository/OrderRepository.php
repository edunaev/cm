<?php

namespace Cargomart\Order\Repository;

use Cargomart\Order\Dto\OrderSearchDto;
use Cargomart\Order\Entity\Order;

/**
 * Repository for searching orders.
 */
class OrderRepository
{
    public function findByHash($hash)
    {
        return new Order();
    }

    /**
     * @param OrderSearchDto $searchDto
     * @return Order[]
     */
    public function search(OrderSearchDto $searchDto)
    {
        return [
            new Order(),
            new Order(),
        ];
    }
}

<?php

namespace Cargomart\Order\Repository;

use Cargomart\Order\Entity\Order;

/**
 * Repository for create/update/delete orders.
 */
class OrderEditRepository
{
    public function create(Order $order)
    {
        return $order;
    }

    public function update(Order $order)
    {
        return $order;
    }
}

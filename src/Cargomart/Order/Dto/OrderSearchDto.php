<?php

namespace Cargomart\Order\Dto;

use Base\RequestInterface;

/**
 * DTO - не обязательно менять от версии к версии.
 * ИМХО, ничего страшного нет в том что здесь может быть много параметров, если они связаны.
 */
class OrderSearchDto
{
    private $from;
    private $to;

    private function __construct($from, $to)
    {
        $this->from = $from;
        $this->to   = $to;
    }

    public static function buildByHttpRequest(RequestInterface $request)
    {
        return new OrderSearchDto(
            (int) $request->getQueryParam('from'),
            (int) $request->getQueryParam('to')
        );
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function getTo()
    {
        return $this->to;
    }
}

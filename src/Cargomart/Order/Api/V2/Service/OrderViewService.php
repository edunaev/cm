<?php

namespace Cargomart\Order\Api\V2\Service;

use Cargomart\Order\Api\V1\Service\OrderViewService as OrderViewServiceV1;
use Cargomart\Order\Entity\Order;

/**
 * Service for output order's data in API v2.
 *
 * Этот сервис вывода наследуются от первой версии.
 * В этой версии вывод карточки остается прежним, но вывод списка меняется: остались только два поля, вместо четырех.
 */
class OrderViewService extends OrderViewServiceV1
{
    /**
     * @param Order $order
     *
     * @return array
     */
    protected function extractListItem(Order $order)
    {
        return [
            'id'   => $order->getId(),
            'name' => $order->getName()
        ];
    }
}

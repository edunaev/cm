<?php

namespace Cargomart\Order\Api\V2\Controller;

use Cargomart\Order\Api\V1\Controller\OrderController as OrderControllerV1;
use Cargomart\Order\Api\V2\Service\OrderViewService as OrderViewServiceV2;

/**
 * Order controller: API v2.
 *
 * Этот контроллер наследуются от контроллера первой версии API, но здесь заменяется view-сервис на новую версию.
 *
 * @see OrderViewServiceV2
 */
class OrderController extends OrderControllerV1
{
    /**
     * @var OrderViewServiceV2
     */
    protected $orderViewService;
}

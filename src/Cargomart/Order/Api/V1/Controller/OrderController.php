<?php

namespace Cargomart\Order\Api\V1\Controller;

use Base\Controller;
use Base\RequestInterface;
use Cargomart\Order\Api\V1\Service\OrderViewService;
use Cargomart\Order\Dto\OrderSearchDto;

/**
 * Order controller: API v1.
 */
class OrderController extends Controller
{
    /**
     * @var OrderViewService
     */
    private $orderViewService;

    /**
     * List.
     */
    public function actionIndex(RequestInterface $request)
    {
        $searchDto = OrderSearchDto::buildByHttpRequest($request);

        return $this->jsonSuccess(
            $this->orderViewService->getOrderList($searchDto)
        );
    }

    /**
     * View.
     */
    public function actionView($hash)
    {
        $order = $this->orderViewService->getOrderByHash($hash);
        if (! $order) {
            return $this->jsonFail('Order not found');
        }

        return $this->jsonSuccess($order);
    }
}

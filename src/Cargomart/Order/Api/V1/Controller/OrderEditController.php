<?php

namespace Cargomart\Order\Api\V1\Controller;

use Base\Controller;
use Base\RequestInterface;
use Cargomart\Order\Api\V1\Service\OrderViewService;
use Cargomart\Order\Porter\JsonOrderPorter;
use Cargomart\Order\Service\OrderEditService;

/**
 * Controller for create/update/delete orders in API v1.
 *
 * Здесь интересно то, что для вывода полной карточки заказа нам достаточно вызвать
 * $this->orderViewService->getOrder($order).
 *
 * Также интересно, что сервис редактирования не возвращает ответ, но в случае фейлов по бизнес-логике выбрасывает
 * LogicException'ы с указанием причины.
 *
 * Вообще, Exception'ы - это обычные классы и их можно расширять для удобства. Например добавить к ним возможность
 * коллекционирования сообщений ошибок (MessageCollectionInterface).
 */
class OrderEditController extends Controller
{
    /**
     * @var JsonOrderPorter
     */
    private $jsonOrderPorter;
    /**
     * @var OrderEditService
     */
    private $orderEditService;
    /**
     * @var OrderViewService
     */
    private $orderViewService;

    /**
     * Create
     */
    public function actionCreate(RequestInterface $request)
    {
        $order = $this->jsonOrderPorter->import($request->getRawBody());

        try {
            $this->orderEditService->createOrder($order);
        } catch (\LogicException $logicException) {
            return $this->jsonFail($logicException->getMessage());
        }

        return $this->jsonSuccess($this->orderViewService->getOrder($order));
    }

    /**
     * Update
     */
    public function actionUpdate(RequestInterface $request)
    {
        $order = $this->jsonOrderPorter->import($request->getRawBody());

        try {
            $this->orderEditService->updateOrder($order);
        } catch (\LogicException $logicException) {
            return $this->jsonFail($logicException->getMessage());
        }

        return $this->jsonSuccess($this->orderViewService->getOrder($order));
    }
}

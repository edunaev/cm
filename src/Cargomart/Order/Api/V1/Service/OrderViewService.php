<?php

namespace Cargomart\Order\Api\V1\Service;

use Cargomart\Order\Dto\OrderSearchDto;
use Cargomart\Order\Entity\Order;
use Cargomart\Order\Repository\OrderRepository;

/**
 * Service for output order's data in API v1.
 */
class OrderViewService
{
    const API_ENTITY_NAME = 'order';

    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * Data for single order.
     */
    public function getOrder(Order $order)
    {
        return [
            self::API_ENTITY_NAME => $this->extract($order)
        ];
    }

    /**
     * Data for single order.
     */
    public function getOrderByHash($hash)
    {
        $order = $this->orderRepository->findByHash($hash);
        if (! $order) {
            return [];
        }

        return $this->getOrder($order);
    }

    /**
     * Data for multiple orders.
     */
    public function getOrderList(OrderSearchDto $searchDto)
    {
        $orders = $this->orderRepository->search($searchDto);
        if (! $orders) {
            return [];
        }

        $result = [];
        foreach ($orders as $order) {
            $result[] = $this->extractListItem($order);
        }

        return [self::API_ENTITY_NAME => $result];
    }

    /**
     * Extracting Order to array.
     */
    protected function extract(Order $order)
    {
        return [
            'id'          => $order->getId(),
            'name'        => $order->getName(),
            'carrierId'   => $order->getCarrierId(),
            'consignorId' => $order->getConsignorId(),
        ];
    }

    /**
     * Extracting Order to array.
     */
    protected function extractListItem(Order $order)
    {
        return $this->extract($order);
    }
}

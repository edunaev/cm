<?php

namespace Cargomart\Order\Porter;

use Cargomart\Order\Entity\Order;

/**
 * Interface for marshalling data to Order.
 *
 * Портеры позволяют из входящих данных формировать знакомые нам объекты (не обязательно сущности, это могут быть DTO).
 * Также их можно использовать и для export'a (extract).
 */
interface IOrderPorter
{
    public function import($data): Order;
}

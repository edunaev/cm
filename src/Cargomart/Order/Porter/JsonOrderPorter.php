<?php

namespace Cargomart\Order\Porter;

use Cargomart\Order\Entity\Order;

/**
 * Marshalling json order object to Order.
 *
 * Реализация для портирования из json'a в Order.
 */
class JsonOrderPorter implements IOrderPorter
{
    public function import($data): Order
    {
        $orderData = json_decode($data, true);
        if (! $orderData) {
            return null;
        }

        return Order::buildByArray($orderData);
    }
}

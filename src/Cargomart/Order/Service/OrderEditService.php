<?php

namespace Cargomart\Order\Service;

use Cargomart\Order\Entity\Order;
use Cargomart\Order\Repository\OrderEditRepository;

class OrderEditService
{
    /**
     * @var OrderEditRepository
     */
    private $orderEditRepository;

    public function __construct(OrderEditRepository $orderEditRepository)
    {
        $this->orderEditRepository = $orderEditRepository;
    }

    public function createOrder(Order $order)
    {
        return $this->orderEditRepository->create($order);
    }

    public function updateOrder(Order $order)
    {
        return $this->orderEditRepository->update($order);
    }
}

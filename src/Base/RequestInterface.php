<?php

namespace Base;

interface RequestInterface
{
    public function getQueryParam($name);

    public function getRawBody();
}

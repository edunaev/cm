<?php

namespace Base;

class Controller
{
    public function jsonFail($error)
    {
        return [];
    }

    public function jsonFailAuth()
    {
        return $this->jsonFail('No auth');
    }

    public function jsonSuccess($data)
    {
        return [];
    }
}
